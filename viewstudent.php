<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="file.css">

    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.min.js"></script>
    <script src="vendor/components/angular.js/angular.min.js"></script>
    <title>School</title>
</head>

<body>
    <div class="container">
        <nav class="navbar navbar-inverse navbar-fixed-top">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#"> ALARA SEC. SCHOOL</a>
                </div>
                <ul class="nav navbar-nav">
                    <li><a href="index.html">Home</a></li>
                    <li><a href="addstudent.php">Add Student</a></li>
                    <li class="active"><a href="viewstudent.php">View Students</a></li>
                    <li><a href="about.html">About</a></li>
                    <li><a href="extra.html">Extra</a></li>

                </ul>
            </div>
        </nav><br><br><br>
        <div class="panel panel-default">
            <div class="panel-heading">Students registered</div>
            <div class="panel-body">
                <table width="100%" height="50%"
                    class=" table timetable_sub  table-bordered table-responsive table-hover table-striped" id="table">
                    <thead class="thead theat-fixed">
                        <tr>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Email Adress</th>
                            <th>Reg.No</th>
                        </tr>
                    </thead>


                    <tbody id="myTable">
                        <?php
         require_once('connection.php');
        $sql="select * from students";
        $result = $conn->query($sql);
        while($row = $result->fetch_assoc()){
            echo "<tr>";
            echo "<td>".$row["firstname"]."</td>";
            echo "<td>".$row["lastname"]."</td>";
            echo "<td>".$row["email"]."</td>";
            echo "<td>".$row["regno"]."</td>";
            echo "</tr>";
        }
            ?>

                    </tbody>

                </table>
            </div>
        </div>
    </div>
    </div>
    </div>

    </div>


    </div>

</body>

</html>