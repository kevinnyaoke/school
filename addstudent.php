<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="file.css">

    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.min.js"></script>
    <script src="vendor/components/angular.js/angular.min.js"></script>
    <title>School</title>
</head>

<body>
    <div class="container">


        <nav class="navbar navbar-inverse navbar-fixed-top">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#"> ALARA SEC. SCHOOL</a>
                </div>
                <ul class="nav navbar-nav">
                    <li><a href="index.html">Home</a></li>
                    <li class="active"><a href="addstudent.php">Add Students</a></li>
                    <li><a href="viewstudent.php">View Students</a></li>
                    <li><a href="about.html">About</a></li>
                    <li><a href="extra.html">Extra</a></li>

                </ul>
            </div>
        </nav>

        <div class="panel panel-default">
            <div class="panel-heading">Submit</div>
            <div class="panel-body">
                <?php
           require_once('connection.php');
           if(isset($_POST['submit'])){
               $firstname=$_POST['firstname'];
               $lastname=$_POST['lastname'];
               $email=$_POST['email'];
               $regno=$_POST['regno'];

               $sql="INSERT INTO students(firstname,lastname,email,regno)VALUES('$firstname','$lastname','$email','$regno')";
               if ($conn->query($sql) === TRUE) {
                echo "<div class='alert alert-success'>";
                 echo " <strong> Success!.</strong> Student registered successfully";
                echo "</div>";
                }
                else{
                echo "<div class='alert alert-danger'>";
                 echo " <strong> Sorry!.</strong> Student is already registered or invalid information";
                echo "</div>";
                }
                
           }
        ?>

                <div class="row">
                    <div class="col-sm-10">
                        <h3>Registration Form</h3><br>
                        <form role="form" method="POST" action="addstudent.php">
                            <div class="form-group">
                                <label for="firstname">First Name:</label>
                                <input type="text" class="form-control" id="firstname" name="firstname" required>
                            </div>
                            <div class="form-group">
                                <label for="lastname">Second Name:</label>
                                <input type="text" class="form-control" id="lastname" name="lastname" required>
                            </div>
                            <div class="form-group">
                                <label for="pwd">Email Adress:</label>
                                <input type="email" class="form-control" id="email" name="email" required>
                            </div>
                            <div class="form-group">
                                <label for="regno">Reg No.:</label>
                                <input type="text" class="form-control" id="regno" name="regno" required>
                            </div>

                            <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                        </form>
                    </div>

                </div>
 
            </div>
        </div>
    </div>
    </div>
    </div>

    </div>

</body>

</html>