var app = angular.module("myApp", []);
app.controller("myCtrl", function ($scope) {

    $scope.first = 1;
    $scope.last = 1;
    $scope.updateValue = function () {
        $scope.calculate = $scope.first + "+" + $scope.last + "=" + (+$scope.first + + $scope.last)
    };

});

app.controller('myCont', function ($scope) {

    $scope.rNum = function () {
        $scope.random = Math.floor(Math.random() * 1000001);
        $scope.randomm = Math.floor(Math.random() * 1000001)
    }


});

app.controller('bMood', function($scope){
    var badmood=['hungry','furious','tired','weak'];
    var goodmood=['fine','full','happy','rich'];

    $scope.myMood=function(){
        $scope.bad=badmood[Math.floor(Math.random()*4)]
        $scope.good=goodmood[Math.floor(Math.random()*4)]
    }

});

app.controller('myGoods', function($scope){
    $scope.goods=[
        {item:'bzzzzzzzooks',price:'ksh.1500'},
        {item:'pencils',price:'ksh.1000'},
        {item:'rubbers',price:'ksh.700'},
        {item:'sets',price:'ksh.2100'},
        {item:'rulers',price:'ksh.900'},
        {item:'desks',price:'ksh.900'},
        {item:'uniform',price:'ksh.900'},
        {item:'socks',price:'ksh.900'},
        {item:'taiss',price:'ksh.900'},
        {item:'lockers',price:'ksh.900'},
        {item:'balls',price:'ksh.900'},
        {item:'dress',price:'ksh.900'},
        {item:'shorts',price:'ksh.900'},
        {item:'trousers',price:'ksh.900'},
        {item:'nailcutters',price:'ksh.900'},
        {item:'jackets',price:'ksh.900'},
    ];

    $scope.getList=function(){
        return $scope.showList ? "olgoods.html":"ulgoods.html"
    }
});

app.controller('myEvents', function($scope){
   $scope.blur=0;
   $scope.click=0;
   $scope.dblclick=0;
   $scope.copy=0;
   $scope.paste=0;
   $scope.cut=0;
   $scope.focus=0;
   $scope.mouseenter=0;
   $scope.mouseleave=0;

//    $scope.disButton=false;
});

app.controller('myStaff', function($scope){
    $scope.staff=[
        {fname:"Clinton",lname:"Omondi",contacts:"0733998877",subjects:"Kiswahili/English"},
        {fname:"Sonford",lname:"Onyango",contacts:"0733998877",subjects:"maths/Science"},
        {fname:"Okoth",lname:"Erick",contacts:"0733998877",subjects:"Geography/ssre"},
        {fname:"Naomi",lname:"Akinyi",contacts:"0733998877",subjects:"history/agric"},
        {fname:"Kevin",lname:"odhiambo",contacts:"0733998877",subjects:"chem/bio"},
        {fname:"kaparo",lname:"fred",contacts:"0733998877",subjects:"geog/English"},
        {fname:"tony",lname:"Omondi",contacts:"0733998877",subjects:"Kiswahili/science"},
        {fname:"jack",lname:"jack",contacts:"0733998877",subjects:"maths/fishing"},
        {fname:"jackob",lname:"jack",contacts:"0733998877",subjects:"welding/driving"}
    ];
});