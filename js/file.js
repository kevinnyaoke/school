function validate() {
    var n = document.getElementById('number').value;
    if (isNaN(n) || n < 1 || n > 20) {
        var text = "Dont joke around";
    } else { 
        text = "You are a good boy";
    }
    document.getElementById('warning').innerHTML = text;
}


function calc() {
    var num1 = parseInt(document.getElementById('fnum').value);
    var num2 = parseInt(document.getElementById('snum').value);

    var oper = document.getElementById('operator').value;

    if (oper === "+") {
        document.getElementById('result').value = num1 + num2;
    }
    if (oper === "-") {
        document.getElementById('result').value = num1 - num2;
    }
    if (oper === "*") {
        document.getElementById('result').value = num1 * num2;
    }
    if (oper === "/") {
        document.getElementById('result').value = num1 / num2;
    }
    if (oper === "%") {
        document.getElementById('result').value = num1 % num2;
    }
}

function add() {
    var numone = parseInt(document.getElementById("num_one").value);
    var numtwo = parseInt(document.getElementById("num_two").value);

    var total=numone + numtwo;
    document.getElementById('sum').value = total;

  }